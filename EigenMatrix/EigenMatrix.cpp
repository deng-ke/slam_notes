#include <iostream>
using namespace std;
#include <ctime>
#include <Eigen/Core>
#include <Eigen/Dense>
#define MATRIX_SIZE 20

int main( int argc, char** argv )
{
/***************************************************************************
 *矩阵、向量的定义和初始化
 ***************************************************************************/
    /*基本定义*/
    Eigen::Matrix<float, 2, 3> matrix_23;
    Eigen::Matrix<float,3,1> vd_3d;
    
    /*动态大小*/
    Eigen::Matrix< double, Eigen::Dynamic, Eigen::Dynamic > matrix_dynamic1;
    Eigen::Matrix< double, 1, Eigen::Dynamic > matrix_dynamic2;
    
    /*简写定义*/
    Eigen::Vector3d v_3d;
    Eigen::MatrixXd matrix_x;
    
    /*初始化*/
    matrix_23 << 1, 2, 3, 4, 5, 6;
    cout << "matrix_23初始化：" << endl << matrix_23 << endl;
    Eigen::Matrix3d matrix_33 = Eigen::Matrix3d::Zero();
    cout << "matrix_33初始化为0：" << endl << matrix_33 << endl;
    matrix_33 = Eigen::Matrix3d::Random();
    cout << "matrix_33初始化为随机数：" << endl << matrix_33 << endl;
    
    /*访问矩阵中的元素*/
    cout << "访问matrix_23中的元素：" << endl;
    for (int i=0; i<2; i++) {
        for (int j=0; j<3; j++)
            cout<<matrix_23(i,j)<<"\t";
        cout<<endl;
    }
    
/***************************************************************************
 *运算
 ***************************************************************************/
    /*四则运算*/
    v_3d << 3, 2, 1;
    vd_3d << 4,5,6;
    Eigen::Matrix<float, 2, 1> result1 = matrix_23 * vd_3d;
    cout << "matrix_23 * vd_3d = " << endl << result1 << endl;
    Eigen::Matrix<double, 2, 1> result2 = matrix_23.cast<double>() * v_3d;
    cout << "matrix_23 * v_3d = " << endl << result2 << endl;  //需显式转换类型
    
    /*其它运算*/
    cout << "转置：" << endl << matrix_33.transpose() << endl;
    cout << "各元素和：" << endl << matrix_33.sum() << endl;
    cout << "迹：" << endl << matrix_33.trace() << endl;
    cout << "数乘：" << endl << 10*matrix_33 << endl;
    cout << "逆：" << endl << matrix_33.inverse() << endl;
    cout << "行列式：" << endl << matrix_33.determinant() << endl;
    //实对称矩阵可保证对角化成功：
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> eigen_solver ( matrix_33.transpose()*matrix_33 );
    cout << "特征值和特征向量：" << endl;
    cout << "Eigen values = \n" << eigen_solver.eigenvalues() << endl;
    cout << "Eigen vectors = \n" << eigen_solver.eigenvectors() << endl;
    
/***************************************************************************
 *解方程：matrix_NN * x = v_Nd
 ***************************************************************************/
    Eigen::Matrix< double, MATRIX_SIZE, MATRIX_SIZE > matrix_NN;
    matrix_NN = Eigen::MatrixXd::Random( MATRIX_SIZE, MATRIX_SIZE );
    Eigen::Matrix< double, MATRIX_SIZE,  1> v_Nd;
    v_Nd = Eigen::MatrixXd::Random( MATRIX_SIZE,1 );
    
    /*直接求逆解方程*/
    clock_t time_stt = clock();
    Eigen::Matrix<double,MATRIX_SIZE,1> x = matrix_NN.inverse()*v_Nd;
    cout << "直接求逆解方程：" << endl;
    cout << "time use=" << endl << 1000* (clock() - time_stt)/(double)CLOCKS_PER_SEC << "ms"<< endl;
    cout << "x=" << endl << x << endl;
    
    /*用QR分解解方程*/
    time_stt = clock();
    x = matrix_NN.colPivHouseholderQr().solve(v_Nd);
    cout << "利用QR分解解方程：" << endl;
    cout << "time use=" << endl << 1000*  (clock() - time_stt)/(double)CLOCKS_PER_SEC <<"ms" << endl;
    cout << "x=" << endl << x << endl;
    return 0;
}